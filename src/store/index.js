import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import thunk from 'redux-thunk'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { reactReduxFirebase,getFirebase } from 'react-redux-firebase'
import firebase from 'firebase'
 
const config = {
  userProfile: 'users'
}
 
const configureStore = (browserHistory, initialState) => {
  const middleware = [thunk.withExtraArgument(getFirebase), routerMiddleware(browserHistory)]

  return createStore(
    connectRouter(browserHistory)(rootReducer),
    initialState,
    compose(
      reactReduxFirebase(firebase, config),
      applyMiddleware(...middleware)
    )
  )
}

export default configureStore  
