import axios from 'axios'
import { ajaxCallError, beginAjaxCall } from '../actions/ajaxCallActions'
import { logOut } from '../actions/loginActions'

export const axiosInstance = axios.create()

export const configureAxios = store =>{

  const headersDeAutenticacao = config => config.headers.Authorization = localStorage.getItem('access_token') ? `Bearer ${localStorage.getItem('access_token')}` : ''

  const requestInterceptor = instanciaAxios => instanciaAxios.interceptors.request.use(
    config => {
      headersDeAutenticacao(config)
      store.dispatch(beginAjaxCall())
      return config
    })
  
  const responseInterceptor = instanciaAxios => instanciaAxios.interceptors.response.use(
    response => response,
    error => {
      store.dispatch(ajaxCallError())

      if (error.response.status === 401)
        store.dispatch(logOut())

      return Promise.reject(error)
    })

  requestInterceptor(axiosInstance)
  responseInterceptor(axiosInstance)
}