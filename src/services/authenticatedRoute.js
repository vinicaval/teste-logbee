import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { isLoggedIn } from './auth'
import PropTypes from 'prop-types'

export const AuthenticatedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={(props) => {
      return isLoggedIn() ? <Component {...props} /> : <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    }
    }
    />
  )
}

AuthenticatedRoute.propTypes = {
  component:PropTypes.func,
  location:PropTypes.string
}