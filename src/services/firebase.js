import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const firebaseConfig = {
  apiKey: 'AIzaSyBXtOCGf7da0SnQHdiN5_Q2Nqh-Riu_xvA',
  authDomain: 'logbee-challenge.firebaseapp.com',
  databaseURL: 'https://logbee-challenge.firebaseio.com',
  storageBucket: 'logbee-challenge.appspot.com',
  projectId:'logbee-challenge'
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const db = firebase.database()
const auth = firebase.auth()

export {
  db,
  auth,
}