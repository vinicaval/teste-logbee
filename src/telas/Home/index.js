import React, { Component } from 'react'
import MapGL, {Marker, Popup, NavigationControl} from 'react-map-gl'
import Pin from '../../components/Pin'
import PropTypes from 'prop-types'
import {db} from '../../services/firebase'

const TOKEN = 'pk.eyJ1IjoidmluaWNhdmFsIiwiYSI6ImNqaW0xNTl0bzJ1b3MzcHF5am9vcnlyaWMifQ.WzBdwJaBT_HudsYM8LxBjg'

const navStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  padding: '10px'
}

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      viewport: {
        latitude: 37.785164,
        longitude: -100,
        zoom: 3.5,
        bearing: 0,
        pitch: 0,
        width: 500,
        height: 500,
      },
      popupInfo: null,
      tasks:[]
    }
  }
  componentDidMount() {
    window.addEventListener('resize', this._resize)
    this._resize()
    db.ref('/tasks').once('value')
      .then(function(dataSnapshot) {
        this.setState({tasks:Array.from(dataSnapshot.val())})
      })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._resize)
  }

  _resize = () => {
    this.setState({
      viewport: {
        ...this.state.viewport,
        width: this.props.width || window.innerWidth,
        height: this.props.height || window.innerHeight
      }
    })
  };

  _updateViewport = (viewport) => {
    this.setState({viewport})
  }

  _renderCityMarker = (task, index) => {
    return (
      <Marker key={`marker-${index}`}
        longitude={task.longitude}
        latitude={task.latitude} >
        <Pin size={20} onClick={() => this.setState({popupInfo: task})} />
      </Marker>
    )
  }

  _renderPopup() {
    const {popupInfo} = this.state

    return popupInfo && (
      <Popup tipSize={5}
        anchor="top"
        longitude={popupInfo.longitude}
        latitude={popupInfo.latitude}
        onClose={() => this.setState({popupInfo: null})} >
      </Popup>
    )
  }


  render() {
    const {viewport, tasks} = this.state
    return (
      <MapGL
        {...viewport}
        mapStyle="mapbox://styles/mapbox/dark-v9"
        onViewportChange={this._updateViewport}
        mapboxApiAccessToken={TOKEN} >

        { tasks && tasks.map(this._renderCityMarker) }      

        {this._renderPopup()}

        <div className="nav" style={navStyle}>
          <NavigationControl onViewportChange={this._updateViewport} />
        </div>
      </MapGL>
    )
  }
}

Home.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number
}