import React, { Component } from 'react'
import { Route } from 'react-router'
import createHistory from 'history/createBrowserHistory'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { AuthenticatedRoute } from '../../services/authenticatedRoute'
import configureStore from '../../store'
import { Login, Home } from '../'
import './styles.scss'
import { configureAxios } from '../../services/axios'

const history = createHistory()
const store = configureStore(history)

configureAxios(store)

export default class Layout extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div>
            <Route exact path='/' component={Login} />
            <Route path='/home' component={Home} />
          </div>
        </ConnectedRouter>
      </Provider>
    )}
}
