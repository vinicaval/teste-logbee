import React, { Component } from 'react'
import { connect } from 'react-redux'
import { login } from '../../actions/loginActions'
import './styles.scss'
import PropTypes from 'prop-types'
import { doSignInWithEmailAndPassword } from '../../services/auth'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loginData: {}
    }
  }

  onChange = event => {
    const { name, value } = event.target
    this.setState({
      loginData: {
        ...this.state.loginData,
        [name]: value
      }
    })
  }

  onLogin = async () => {
    const { email, password } = this.state.loginData

    doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.props.login(this.state)
      })
  }

  render() {
    const inputEvents = {
      onChange: this.onChange,
    }
    const {
      loginData: { email = '', password = '' } } = this.state

    return (
      <div className="container">
        <h1 className="title">Teste Logbee</h1>
        <div className="box">
          <div className="has-text-centered">
            <h2>Faça seu login:</h2>
          </div>
          <div className="column">
            <div className="field">
              <label className="label" htmlFor="email">
                Email
              </label>
              <div className="control">
                <input
                  className="input"
                  name="email"
                  value={email}
                  type="email"
                  {...inputEvents} />
              </div>
            </div>
            <div className="field">
              <label className="label" htmlFor="password">
                Senha
              </label>
              <div className="control">
                <input
                  className="input"
                  name="password"
                  value={password}
                  type="password"
                  {...inputEvents} />
              </div>
            </div>
          </div>
          <div className="field  is-grouped is-grouped-centered">
            <div className="control">
              <button onClick={this.onLogin} className="button is-info is-medium">Entrar</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = undefined

const mapDispatchToProps = dispatch => ({
  login: loginData => dispatch(login(loginData))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)

Login.propTypes = {
  login: PropTypes.func
}