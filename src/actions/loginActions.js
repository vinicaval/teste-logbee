import { push } from 'react-router-redux'
import * as actionTypes from './actionTypes'
import { logoff } from '../services/auth'

export const loginSuccess = loginData =>
  ({ type: actionTypes.LOGIN_SUCCESS, loginData })

export const logOut = () =>
  ({ type: actionTypes.LOG_OUT })

export const login = loginData =>
  async dispatch => {
    dispatch(loginSuccess({...loginData}))

    dispatch(push('/home'))
  }

export const LogOut = () =>
  dispatch => {
    logoff()
    dispatch(logOut())
    dispatch(push('/'))
  }