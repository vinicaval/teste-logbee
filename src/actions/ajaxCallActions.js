import { BEGIN_AJAX_CALL, AJAX_CALL_ERROR, END_AJAX_CALL } from './actionTypes'

export const beginAjaxCall = () => ({ type: BEGIN_AJAX_CALL })
export const ajaxCallError = () => ({ type: AJAX_CALL_ERROR })
export const endAjaxCall = () => ({ type: END_AJAX_CALL })