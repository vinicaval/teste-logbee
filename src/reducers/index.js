import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'
import { firebaseReducer } from 'react-redux-firebase'
import login from './login'
import ajaxCall from './ajaxCall'

const appReducer = combineReducers({
  login,
  ajaxCall,
  firebaseReducer,
  routing
})

const rootReducer = (state, action) => {
  if (action.type === 'LOG_OUT')
    state = undefined

  return appReducer(state, action)
}

export default rootReducer