import * as actionTypes from '../actions/actionTypes'
import initialState from './initialstate'

export default function login(state = initialState.login, action) {
  switch (action.type) {
  case actionTypes.LOGIN_SUCCESS:
    return {
      ...action.loginData
    }
  default:
    return state
  }
}