import * as types from '../actions/actionTypes'
import initialState from './initialstate'

const actionTypeEndsInSuccess = type => type.substring(type.length - 8) === '_SUCCESS'

export default (state = initialState.ajaxCall, action) => {
  if (action.type === types.BEGIN_AJAX_CALL)
    return state + 1
  else if (action.type === types.AJAX_CALL_ERROR || actionTypeEndsInSuccess(action.type) || action.type === types.END_AJAX_CALL)
    return state === 0 ? 0 : state - 1

  return state
}
