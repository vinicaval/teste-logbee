/* eslint-disable */
import { shallow, render, mount } from 'enzyme'
import moxios from 'moxios'

global.shallow = shallow
global.render = render
global.mount = mount
global.moxios = moxios
global.event = (name, value) => {
  return { target: { name, value } }
}
global.localStorage=(function() {
  var store = {}
  return {
    getItem: function(key) {
      return store[key]
    },
    setItem: function(key, value) {
      store[key] = value
    },
    clear: function() {
      store = {}
    },
    removeItem: function(key) {
      delete store[key]
    }
  }
})()
console.error = message => {
  if (!message.includes('Shallow renderer has been moved to react-test-renderer/shallow'))
    console.log(message)
}