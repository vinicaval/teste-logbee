/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = (env) => {
    const isDevBuild = !(env && env.prod);
    return [{
        mode: isDevBuild?"development":"production",
        stats: { modules: false },
        entry: './src/app.js',
        resolve: { extensions: ['.js', '.jsx'] },
        output: {
            path: path.join(__dirname, 'dist'),
            filename: '[name].js',
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    query: {
                        presets: ['react', 'es2015'],
                        plugins: ['transform-class-properties']
                    }
                },
                { test: /\.(png|jpg|jpeg|gif)$/, use: 'url-loader?limit=25000' },
                { test: /\.(s|)css$/, use: ExtractTextPlugin.extract({ use: [isDevBuild ? 'css-loader' : 'css-loader?minimize', 'sass-loader'] }) }
            ]
        },
        plugins: [
            new ExtractTextPlugin('site.css'),
            new WebpackNotifierPlugin()
        ].concat(isDevBuild ? [
            // Plugins that apply in development builds only
            new webpack.SourceMapDevToolPlugin({
                filename: '[file].map', // Remove this line if you prefer inline source maps
                moduleFilenameTemplate: path.relative('dist', '[resourcePath]') // Point sourcemap entries to the original file locations on disk
            })
        ] : [
                // Plugins that apply in production builds only
                new webpack.optimize.UglifyJsPlugin()
            ])
    }];
};